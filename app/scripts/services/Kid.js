(function () {
    'use strict';

    angular
        .module('theListApp')
        .factory('Kid', Kid);

    function Kid() {

        function Kid(niceness, name, wish){
            this.nice = niceness || false;
            this.name = name || 'Add Name...';
            this.wish = wish || 'Add Wish...';
        }

        return Kid;

    }

})();