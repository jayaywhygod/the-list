(function () {
    'use strict';

    angular
        .module('theListApp')
        .factory('NaughtyOrNiceModel', NaughtyOrNiceModel);

    NaughtyOrNiceModel.$inject = ['$rootScope','$location', '$timeout', 'Kid'];

    function NaughtyOrNiceModel($rootScope, $location, $timeout, Kid) {
        var naughtyOrNiceModel = [
            {
                nice:true,
                name:'Matt',
                wish:'A Front End Dev'
            },
            {
                nice:true,
                name:'Josh',
                wish:'Video Games'
            },
            {
                nice:true,
                name:'Jessi',
                wish:'Warm Things'
            },
            {
                nice:false,
                name:'Beavis',
                wish:'TP for Bunghole'
            },
            {
                nice:false,
                name:'Buffalo Bill',
                wish:'New Suit'
            }
        ];

        var service = {
            get: get,
            addKid: addKid,
            removeKid: removeKid,
            exportJSON: exportJSON
        };
        return service;

        // IMPLEMENTATION DETAILS

        function get(callback) {
            if (callback){
                callback(naughtyOrNiceModel);
            } else {
                return naughtyOrNiceModel;
            }
        }

        function addKid(list, niceness, name, wish, callback){
            $rootScope.$broadcast('displayNaughtyOrNiceList');
            if (!$location.search().naughty){
                niceness = true;
            }
            var newKid = new Kid(niceness,name,wish);
            list.splice(0,0,newKid);
            if (callback){
                callback(list);
            }
        }

        function removeKid(thisList, thisKid, callback, delay){
            var delay = delay || 0;
            if (thisKid){
                thisKid.deleting = true;
                $timeout(function(){
                    for (var i = 0; i < thisList.length; i++) {
                        var kid = thisList[i];
                        if (kid.name === thisKid.name){
                            thisList.splice(i,1);
                            if (callback){
                                callback(thisList);
                                return;
                            }
                        }
                    }
                },delay);
            }
        }

        function exportJSON(list){
            var exportedNaughtyOrNiceList = {nice:[],naughty:[]};
            for(var i = 0; i < list.length; i++) {
                var kid = list[i];
                if (kid.nice) {
                    exportedNaughtyOrNiceList.nice.push(kid);
                } else {
                    exportedNaughtyOrNiceList.naughty.push(kid);
                }
                if (i === list.length - 1){
                    return exportedNaughtyOrNiceList;
                }
            }
        }
    }

})();