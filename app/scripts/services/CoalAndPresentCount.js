(function () {
    'use strict';

    angular
        .module('theListApp')
        .factory('CoalAndPresentCount', CoalAndPresentCount);

    function CoalAndPresentCount() {
        var CountService = {
            get: get,
            update: update
        };
        return CountService;


        // IMPLEMENTATION DETAILS
        function get(sourceList){
            return update(sourceList);
        }

        function update(sourceList) {
            var count = {
                coal:0,
                presents:0
            };
            if (!sourceList || sourceList.length === 0){
                return count;
            }
            for(var i = 0; i < sourceList.length; i++) {
                var kid = sourceList[i];
                if (kid.nice && kid.wish !== 'Add Wish...') {
                    count.presents++;
                } else if (!kid.nice) {
                    count.coal++;
                }
                if (i === sourceList.length - 1){
                    return count;
                }
            }
        }
    }

})();