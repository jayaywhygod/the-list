'use strict';

/**
 * @ngdoc overview
 * @name theListApp
 * @description
 * # theListApp
 *
 * Main module of the application.
 */
angular
    .module('theListApp', [
        'ngAnimate',
        'ngTouch',
        'filterButtons',
        'shoppingList'
    ]);