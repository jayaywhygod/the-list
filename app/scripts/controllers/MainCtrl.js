(function () {
    'use strict';

    angular
        .module('theListApp')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$rootScope','$timeout','NaughtyOrNiceModel', 'CoalAndPresentCount'];

    function MainCtrl($rootScope,$timeout,NaughtyOrNiceModel, CoalAndPresentCount) {

        // Why Use 'mainCtrl' As An Alias For  $scope?
        // -------------------------------------------
        // namespacing via controllerAs allows clearer scoping when nesting controllers/components
        // it also prevents simple primitives from creating new local scopes
        // https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md#controllers
        // John Papas advocates for using vm as the scope alias in simple cases,
        // but this convention becomes less clear as the application scales up,
        // and using the controller name camel-cased helps eliminate confusion

        var mainCtrl = this;
            mainCtrl.addToNaughtyOrNiceList         = addToNaughtyOrNiceList;
            mainCtrl.removeFromNaughtyOrNiceList    = removeFromNaughtyOrNiceList;
            mainCtrl.toggleNaughtyOrNice            = toggleNaughtyOrNice;
            mainCtrl.enableNameEditing              = enableNameEditing;
            mainCtrl.nameInputKeyListener           = nameInputKeyListener;
            mainCtrl.cancelNameEdit                 = cancelNameEdit;
            mainCtrl.saveNameEdit                   = saveNameEdit;
            mainCtrl.enableWishEditing              = enableWishEditing;
            mainCtrl.wishInputKeyListener           = wishInputKeyListener;
            mainCtrl.clearDefaultWish               = clearDefaultWish;
            mainCtrl.saveWishEdit                   = saveWishEdit;
            mainCtrl.cancelWishEdit                 = cancelWishEdit;
            mainCtrl.setUndoHistory                 = setUndoHistory;
            mainCtrl.getUndoHistory                 = getUndoHistory;

            mainCtrl.coalAndPresentCount            = CoalAndPresentCount.get();
            mainCtrl.shoppingList                   = {visible:false, presents:[]};

            mainCtrl.undoHistory                    = {};
            mainCtrl.filterOut                      = '';
            mainCtrl.selectedFilter                 = '';


        $timeout(function(){
            NaughtyOrNiceModel.get(function(response){
                mainCtrl.naughtyOrNiceModel = response;
                updateCoalAndPresentCount(mainCtrl.naughtyOrNiceModel);
            });
        });

        // IMPLEMENTATION DETAILS

        function updateCoalAndPresentCount(list){
            mainCtrl.coalAndPresentCount = CoalAndPresentCount.get(list);
        }

        function addToNaughtyOrNiceList(list, niceness, name, wish){
            if (mainCtrl.selectedFilter === 'export') {
                mainCtrl.selectedFilter = '';
                $rootScope.$broadcast('newAdditionToNaughtyOrNiceList');
            }
            NaughtyOrNiceModel.addKid(list, niceness, name, wish, function(updatedList){
                updateCoalAndPresentCount(updatedList);
            });
        }

        function removeFromNaughtyOrNiceList(list, kid){
            NaughtyOrNiceModel.removeKid(list, kid, function(updatedList){
                updateCoalAndPresentCount(updatedList);
            }, 300);
        }

        function toggleNaughtyOrNice(list, kid) {
            if (kid){
                kid.nice = !kid.nice;
                updateCoalAndPresentCount(list);
            }
        }

        function clearName(kid){
            if (kid){
                kid.name = '';
            }
        }

        function enableNameEditing(kid, previousState){
            if (kid){
                setUndoHistory('unsavedName', previousState || kid.name);
                kid.$nameEditing = true;
                if (kid.name === 'Add Name...'){
                    clearName(kid);
                }
            }
        }

        function saveNameEdit(kid){
            if (kid){
                delete kid.$nameEditing;
                updateCoalAndPresentCount(mainCtrl.naughtyOrNiceModel);
            }
        }

        function cancelNameEdit(kid){
            if (kid){
                var previousState = getUndoHistory('unsavedName');
                if (previousState) {
                    kid.name = previousState;
                }
                delete kid.$nameEditing;
            }
        }

        function nameInputKeyListener(kid, ngEvent){
            if (ngEvent && ngEvent.keyCode == 27) {
                ngEvent.currentTarget.blur();
                cancelNameEdit(kid);
            } else if (ngEvent && ngEvent.keyCode === 13) {
                ngEvent.currentTarget.blur();
            }
        }

        function setUndoHistory(propertyName, currentValue){
            mainCtrl.undoHistory[propertyName] = angular.copy(currentValue);
        }

        function getUndoHistory(propertyName){
            return mainCtrl.undoHistory[propertyName];
        }

        function enableWishEditing(kid){
            if (kid && kid.nice){
                setUndoHistory('unsavedWish', kid.wish);
                kid.$wishEditing = true;
                clearDefaultWish(kid);
            }
        }

        function saveWishEdit(kid){
            if (kid){
                delete kid.$wishEditing;
                updateCoalAndPresentCount(mainCtrl.naughtyOrNiceModel);
            }
        }

        function clearDefaultWish(kid){
            if (kid && kid.wish === 'Add Wish...'){
                setUndoHistory('unsavedWish', kid.wish);
                kid.wish = '';
            }
        }

        function cancelWishEdit(kid){
            if (kid){
                kid.wish = getUndoHistory('unsavedWish');
                delete kid.$wishEditing;
            }
        }

        function wishInputKeyListener(kid, ngEvent){
            if (ngEvent && ngEvent.keyCode == 27){
                ngEvent.currentTarget.blur();
                cancelWishEdit(kid);
            }  else if (ngEvent && ngEvent.keyCode === 13) {
                ngEvent.currentTarget.blur();
            }
        }
    }

})();