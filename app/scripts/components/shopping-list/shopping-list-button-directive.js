(function() {
    'use strict';

    /**
     * @ngdoc service
     * @name shoppingList.shoppingListButton
     * @description
     * # shoppingListButton
     * Factory in the shoppingList.
     */

    angular
        .module('shoppingList')
        .directive('shoppingListButton', ShoppingListButtonDirective);

    function ShoppingListButtonDirective() {
        var directive = {
            restrict: 'EA',
            template:
                '<div ' +
                    'ng-click="shoppingListButtonCtrl.displayShoppingList()"'+
                    'title="Click to Shop for {{shoppingListButtonCtrl.presentsCount}} Presents in Shopping List"'+
                    'class="present-counter" >'+
                    '<div class="present-counter-inner-container">'+
                        '<span class="present-counter-icon ti-shopping-cart"></span>'+
                        '<div ng-bind="shoppingListButtonCtrl.presentsCount" class="present-count"></div>'+
                    '</div>'+
                '</div>',
            scope: {
                presentsCount:'='
            },
            bindToController: true,
            controller: ShoppingListButtonCtrl,
            controllerAs: 'shoppingListButtonCtrl'
        };

        return directive;
    }

    ShoppingListButtonCtrl.$inject = ['$rootScope'];

    function ShoppingListButtonCtrl($rootScope) {
        var shoppingListButtonCtrl                      = this;
            shoppingListButtonCtrl.displayShoppingList  = displayShoppingList;

        function displayShoppingList(){
            $rootScope.$broadcast('displayShoppingList');
        }

    }

})();