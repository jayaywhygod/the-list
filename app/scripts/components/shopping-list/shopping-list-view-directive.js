(function() {
    'use strict';

    /**
     * @ngdoc service
     * @name shoppingList.shoppingListView
     * @description
     * # shoppingListView
     * Factory in the shoppingList.
     */

    angular
        .module('shoppingList')
        .directive('shoppingListView', ShoppingListViewDirective);

    function ShoppingListViewDirective() {
        var directive = {
            restrict: 'EA',
            template:
                '<a '+
                    'ng-repeat="present in shoppingListViewCtrl.shoppingList.presents"'+
                    'ng-href="https://www.amazon.com/s/?field-keywords={{present.wish}}"'+
                    'ng-show="shoppingListViewCtrl.shoppingList.visible"'+
                    'ng-class="{\'hiding\':shoppingListViewCtrl.shoppingList.hiding}"'+
                    'class="shopping-list-link"'+
                    'title="Shop for \'{{present.wish}}\' on Amazon"'+
                    'target="_blank">'+
                    '<span class="present-for-label">Shop For {{present.name}}</span>'+
                    '<br>'+
                    '<span ng-bind="present.wish" class="present-wish-label"></span>'+
                    '<span class="present-shopping-link-icon ti-angle-right"></span>'+
                '</a>',
            scope: {
                naughtyOrNiceList:'=',
                shoppingList:'='
            },
            bindToController: true,
            controller: ShoppingListButtonCtrl,
            controllerAs: 'shoppingListViewCtrl'
        };

        return directive;
    }

    ShoppingListButtonCtrl.$inject = ['$rootScope', '$scope', '$timeout'];

    function ShoppingListButtonCtrl($rootScope, $scope, $timeout) {
        var shoppingListViewCtrl                = this;

        $scope.$on('displayNaughtyOrNiceList', hideShoppingList);
        $scope.$on('displayShoppingList', viewShoppingList);



        // IMPLEMENTATION DETAILS

        function viewShoppingList(){
            shoppingListViewCtrl.shoppingList.hiding = false;
            shoppingListViewCtrl.shoppingList.visible = true;
            populateShoppingList(shoppingListViewCtrl.naughtyOrNiceList);
            $rootScope.$broadcast('deselectFilterButtons');
        }

        function hideShoppingList(){
            shoppingListViewCtrl.shoppingList.hiding = true;
            $timeout(function(){
                shoppingListViewCtrl.shoppingList.visible = false;
                initShoppingList();
            },150);
        }

        function initShoppingList(callback){
            shoppingListViewCtrl.shoppingList.presents = [];
            if (callback){
                callback();
            }
        }

        function populateShoppingList(list){
            initShoppingList(function(){
                for(var i = 0; i < list.length; i++) {
                    var kid = list[i];
                    if (kid.nice && kid.wish !== 'Add Wish...'){
                        shoppingListViewCtrl.shoppingList.presents.push(kid);
                    }
                }
            });
        }

    }

})();