(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name filterButtons.filterButtons
     * @description
     * # filterButtons
     * filterButtons component directive
     *
     * uses inline template string due to CORS problems that occur
     * when running angular apps locally in Chrome without a localserver;
     * local server would require an additional build step,
     * which is outside of the scope of this task
     *
     */

    angular
         .module('filterButtons')
         .directive('filterButtons', FilterButtons);

    function FilterButtons() {
        var directive = {
            restrict: 'EA',
            template:
                '<div class="naughty-or-nice-filters">' +
                    '<div ' +
                        'ng-repeat="filter in filterButtonsCtrl.filterButtons"'+
                        'ng-click="filterButtonsCtrl.selectFilter(filterButtonsCtrl.listModel, filter)"'+
                        'ng-class="{\'active\':filter.active}"' +
                        'ng-bind="filter.text"'+
                        'ng-cloak>'+
                    '</div>'+
                '</div>',
            scope: {
                selectedFilter: '=',
                listModel:'=',
                listExport:'='
            },
            bindToController: true,
            controller: FilterButtonsCtrl,
            controllerAs: 'filterButtonsCtrl'
        };

        return directive;
    }

    FilterButtonsCtrl.$inject = ['$rootScope', '$scope', '$timeout','$location','FilterButtons', 'NaughtyOrNiceModel'];

    function FilterButtonsCtrl($rootScope, $scope, $timeout, $location, FilterButtons, NaughtyOrNiceModel) {
        var filterButtonsCtrl               = this;
            filterButtonsCtrl.filterButtons = FilterButtons.get();
            filterButtonsCtrl.selectFilter  = selectFilter;

        // NOTE: only broadcast from outside component
        $scope.$on('deselectFilterButtons', function(){
            deselectFilters(filterButtonsCtrl.filterButtons);
        });
        $scope.$on('newAdditionToNaughtyOrNiceList', function(){
            if (!$location.search().naughty){
                deselectFilters(filterButtonsCtrl.filterButtons, 'All');
            }
        });

        if ($location.search().naughty){
            $location.search('naughty', null);
        }

        // IMPLEMENTATION DETAILS

        function selectFilter(list, targetFilter){
            if (targetFilter){
                $rootScope.$broadcast('displayNaughtyOrNiceList');
                animateListPopulation(list);
                targetFilter.active = true;
                filterButtonsCtrl.selectedFilter = targetFilter.filterIfNiceIsntThis;
                if (typeof filterButtonsCtrl.selectedFilter === "boolean" && !filterButtonsCtrl.selectedFilter){
                    $location.search('naughty', true);
                } else {
                    $location.search('naughty', null);
                }
                if (filterButtonsCtrl.selectedFilter === 'export'){
                    filterButtonsCtrl.listExport = NaughtyOrNiceModel.exportJSON(list);
                }
                deselectFilters(filterButtonsCtrl.filterButtons, targetFilter.text);
            }
        }

        function deselectFilters(filters, targetFilterText){
            for(var i = 0; i < filters.length; i++) {
                if (filters[i].active && (filters[i].text !== targetFilterText)){
                    filters[i].active = false;
                } else if (filters[i].text === targetFilterText) {
                    filters[i].active = true;
                }
            }
        }

        function animateListPopulation(list){
            filterButtonsCtrl.listModel = [];
            $timeout(function(){
                filterButtonsCtrl.listModel = list;
            });
        }
    }

})();