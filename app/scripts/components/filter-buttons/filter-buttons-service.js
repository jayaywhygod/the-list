(function () {
    'use strict';

    angular
        .module('filterButtons')
        .factory('FilterButtons', FilterButtons);

    function FilterButtons() {
        var filters = [
            {
                text: 'All',
                active:true
            },
            {
                text: 'Nice',
                active: false,
                filterIfNiceIsntThis:true
            },
            {
                text: 'Naughty',
                active: false,
                filterIfNiceIsntThis:false
            },
            {
                text: 'Export',
                active: false,
                filterIfNiceIsntThis:'export'
            }
        ];

        var service = {
            get: get
        };
        return service;

        // IMPLEMENTATION DETAILS

        function get() {
            return filters;
        }
    }

})();