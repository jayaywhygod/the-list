'use strict';

/**
 * @ngdoc overview
 * @name filterButtons
 * @description
 *
 * # filterButtons Component
 *
 * standalone component can be included as filter-buttons element or attribute
 * inject into main app module and supply required attributes to template:
 *
 * <filter-buttons
 *      list-model="mainCtrl.naughtyOrNiceModel"
 *      list-export="mainCtrl.exportedNaughtyOrNiceList"
 *      selected-filter="mainCtrl.selectedFilter">
 * </filter-buttons>
 *
 */

angular
    .module('filterButtons', []);