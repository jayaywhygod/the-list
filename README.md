# README #

### Important Note About this Submission ###

Per the project's requirements, I've put this together in a way that doesn't require any additional build steps. 

**There is one issue with that**: Angular loads templates (ngRoutes, ngView, directive templateUrl, etc) using XHR requests under the hood, which Chrome won't allow unless served from a localserver that enables CORS requests when running locally. A localserver instance would require an additional build step, which the project instructions explicitly forebode, so I bypassed ngRoutes and embedded the main view template directly into index.html because it's important to me that the app function the same in all browsers. 

I didn't componentize the project quite as much as I normally would due to the awkwardness of the CORS issues and wanting to get the project completed in a reasonable amount of time, so the project isn't _fully_ componentized, but I did componentize the <filter-buttons> and <shopping-list> components as a examples of my component development skills, and I did move most of the application logic into services the way that I normally would. In order to componentize the filter-buttons/shopping-list without using XHR, I had to embed the directive templates as template strings in the directive definition object instead of using a templateUrl and separate html file, which is a little less clean, but still gets the job done :)


####That being said, I actually had a lot of fun with this project (probably too much haha), and I'm pretty happy with the app that I put together!####



# About My Components #

My approach to componentization is based on  [the google angular directory styleguide](https://docs.google.com/document/d/1XXMvReO8-Awi1EZXAXS4PzDzdNvV6pGcuaF4Q9821Es/pub). 

#### Normally I split up each piece of functionality like so, per [the google angular directory styleguide](https://docs.google.com/document/d/1XXMvReO8-Awi1EZXAXS4PzDzdNvV6pGcuaF4Q9821Es/pub):

```
 app/
    components/
        filter-buttons/
            filter-buttons-module.js
            filter-buttons-directive.js
            filter-buttons-service.js
            filter-buttons-template.html
        shopping-list/
            shopping-list-module.js
            shopping-list-button-directive.js
            shopping-list-button-template.html
            shopping-list-view-directive.js
            shopping-list-view-template.html
        etc...


```

Normally I would then concatenate and minify/uglify each components pieces into a single file and utilize $templateCache for any html templates so that each piece can be as optimized and reusable as possible in multiple apps and distributed as a bower_component.

# About the Design #
I chose a Flat UI aesthetic and designed with a mobile-first approach. Given that the app's target audience was slightly older, functionality seemed like it should be simple and app-like regardless of environment, with minimal, unobtrusive UI elements and as few clicks as possible for an uncluttered experience. 

In keeping with a mobile app experience, it seemed like creating a separate form would only create the need for additional clicks and frustration, so I incorporated the create/edit form into the display view for a seemless, tactile experience rather than separating.

For additional convenience, the present counter in the top right can be clicked to view a shopping list, and each item in the shopping list can be clicked to shop for that item on Amazon by passing the item name as a query parameter.

# About the Code Style #

## HTML ##
The HTML throughout the project adheres to best practices found in many open source projects, in which it is preferred for each line to be around 80-100 characters long or less. This is ideal for collaboration and helps accommodate a variety of screen widths, especially laptop users working in editors with a project pane that takes up precious real estate or VIM users working in a linux terminal. 

HTML elements with Angular decorators can get particularly long which would normally require a lot of scrolling in the laptop example, or force unreadable wrapping in a VIM/terminal example. To accommodate, I indent attributes and add all attributes to new lines. 

However, The goal is to facilitate effective collaboration - this isn't something that I'm married to and I could easily stop doing so if other project members dislike the style. 

EXAMPLE:
```
<!-- Super wide, harder to read as more angular decorators and css classes are added to the element -->
<div ng-repeat="filter in mainCtrl.filters" ng-click="mainCtrl.selectFilter(filter)" ng-class="{'active':filter.active}" ng-cloak>{{filter.text}}</div>



<!-- 
   Easier to read when less real estate width is available, 
   especially as more decorators/classes are added;
   I typically only employ when there are multiple angular decorators,
   otherwise it can make simpler elements harder to read
-->

<div
  ng-repeat="filter in mainCtrl.filters"
  ng-click="mainCtrl.selectFilter(filter)"
  ng-class="{'active':filter.active}"
  ng-bind="filter.text"
  ng-cloak>
</div>
```

## JS ##
I've found these methodologies very helpful for long term code maintenance, and while they have a status of "Best Practice" in many circles, I'm not dogmatic about their usage and I'm totally willing to adopt a team's preferred code style.

I employ **[John Papa's Angular Styleguide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md)** for angular coding, especially the **revealing module pattern**, as well as approaches gleaned from textbooks like **Eloquent JavaScript**. I believe that these work hand in hand and are ideal for group collaboration. They can really enhance readability, which is important when working on a team (or when an app needs to be updated 6+ months down the road and you've switched contexts or didn't document your original intents). **Additionally the John Papa guide has been officially endorsed by the Angular team**.

**Eloquent JavaScript** encourages verbosity and readability, as the application's purpose may evolve over time and scale up in terms of functionality and contributing users. For example, while it may initially be apparent what an "addToList()" function is accomplishing in the initial stage, that may be less apparent in 6 months' time when there are multiple lists (naughty or nice, user list, shopping list, etc), so consistent verbosity can create an API that will stand up better over time and require less refactoring of variable and function names. Verbosity also allows for a very plain English reading experience that ideally can be understand by new collaborators, backend programmers, or even by non-coders.

**John Papa's Guide** encourages usage of the **revealing module pattern** for readability and ease of refactoring. It is an approach that should look familiar to Java programmers, as variables and bindables are declared at the top of the file and the implementation details of those functions are "revealed" later on in the file so that you can see at a glance how the functions and variables interact without being bogged down in the details until necessary.


## About The App's Performance ##
* angular.forEach is nice and clean, but I use plain old vanilla JavaScript for loops instead for performance reasons: [JavaScript for loops can be 20 times as fast as angular.forEach](https://jsperf.com/angular-foreach-vs-underscore-foreach/43)

* I use ng-bind in most instances over the {{curly expressions}}, as ng-bind will prevent flashes of unstyled content since the attributes won't render to the view until Angular has had a chance to scoop up and parse any ng- attributes.

* I would normally use grunt and ngTemplates/$template cache to concat/compress/cache templates for better performance, but skipped those steps for the sake of code evaluation.

# THANKS!
Thanks so much taking the time to review my code, I had a blast putting it together! ...Judging by the length of this Read Me, I probably had way too much fun and owe you an even bigger thanks for trying to read this novel!